
Supersized Formatter 7.x-1.x
--------------------------

Install
-------
* Enable the module
* Download The Supersized plugin (http://buildinternet.com/project/supersized/) 
* and put it in sites/all/libraries
* Set image display in Manage display to Supersized

Description
-----------
This module allows for integration of Supersized core functionality into Drupal 
as image formatter.

Author
------
kszyhu
