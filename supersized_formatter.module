<?php

/**
 * @file
 * The module provides image formatter to display it on background of the page
 * using Supersized jQuery plugin
 */

/**
 * Implements hook_field_formatter_info().
 */
function supersized_formatter_field_formatter_info() {
  return array(
    'supersized' => array(
      'label' => t('Supersized'),
      'field types' => array('image'),
      'settings' => array(
        'start_slide' => 1,
        'new_window' => 1,
        'image_protect' => 1,
        'min_height' => 0,
        'min_width' => 0,
        'fit_always' => 0,
        'fit_landscape' => 0,
        'fit_portrait' => 1,
        'horizontal_center' => 1,
        'vertical_center' => 1,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function supersized_formatter_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  // This gets the view mode where our settis are stored.
  $display = $instance['display'][$view_mode];
  // This gets the actual settings.
  $settings = $display['settings'];
  // Initialize the element veriable.
  $element = array();

  $element['start_slide'] = array(
    '#title' => t('Start slide'),
    '#type' => 'textfield',
    '#size' => 10,
    '#description' => t('The slide the slideshow starts on. *In the Core version, it controls which image is loaded, 0 causes a random image to be loaded each time.'),
    '#default_value' => $settings['start_slide'],
  );

  $element['new_window'] = array(
    '#title' => t('New window'),
    '#type' => 'checkbox',
    '#description' => t('Slide links open in a new window.'),
    '#default_value' => $settings['new_window'],
  );

  $element['image_protect'] = array(
    '#title' => t('Image protect'),
    '#type' => 'checkbox',
    '#description' => t('Disables right clicking and image dragging using Javascript.'),
    '#default_value' => $settings['image_protect'],
  );

  $element['min_height'] = array(
    '#title' => t('Min height'),
    '#type' => 'textfield',
    '#size' => 10,
    '#description' => t("Minimum height the image is allowed to be. If it is met, the image won't size down any further."),
    '#default_value' => $settings['min_height'],
  );

  $element['min_width'] = array(
    '#title' => t('Min width'),
    '#type' => 'textfield',
    '#size' => 10,
    '#description' => t("Minimum width the image is allowed to be. If it is met, the image won't size down any further."),
    '#default_value' => $settings['min_width'],
  );

  $element['fit_always'] = array(
    '#title' => t('Fit always'),
    '#type' => 'checkbox',
    '#description' => t('Prevents the image from ever being cropped. Ignores minimum width and height.'),
    '#default_value' => $settings['fit_always'],
  );

  $element['fit_landscape'] = array(
    '#title' => t('Fit landscape'),
    '#type' => 'checkbox',
    '#description' => t('Prevents the image from being cropped by locking it at 100% width.'),
    '#default_value' => $settings['fit_landscape'],
  );

  $element['fit_portrait'] = array(
    '#title' => t('Fit portrait'),
    '#type' => 'checkbox',
    '#description' => t('Prevents the image from being cropped by locking it at 100% height.'),
    '#default_value' => $settings['fit_portrait'],
  );


  $element['horizontal_center'] = array(
    '#title' => t('Horizontal center'),
    '#type' => 'checkbox',
    '#description' => t('Centers image horizontally. When turned off, the images resize/display from the left of the page.'),
    '#default_value' => $settings['horizontal_center'],
  );

  $element['vertical_center'] = array(
    '#title' => t('Vertical center'),
    '#type' => 'checkbox',
    '#description' => t('Centers image vertically. When turned off, the images resize/display from the top of the page.'),
    '#default_value' => $settings['vertical_center'],
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function supersized_formatter_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = array();

  $summary[] = t('Start slide: @start_slide', array('@start_slide' => $settings['start_slide']?'true' : 'false'));
  $summary[] = t('Image protect: @image_protect', array('@image_protect' => $settings['image_protect']?'true' : 'false'));
  $summary[] = t('Vertical center: @vertical_center', array('@vertical_center' => $settings['vertical_center']?'true' : 'false'));
  $summary[] = t('Horizontal center: @horizontal_center', array('@horizontal_center' => $settings['horizontal_center']?'true' : 'false'));
  $summary[] = t('Min width: @min_width', array('@min_width' => $settings['min_width']));
  $summary[] = t('Min height: @min_height', array('@min_height' => $settings['min_height']));
  $summary[] = t('Fit always: @fit_always', array('@fit_always' => $settings['fit_always']?'true' : 'false'));
  $summary[] = t('Fit portrait: @fit_portrait', array('@fit_portrait' => $settings['fit_portrait']?'true' : 'false'));
  $summary[] = t('Fit landscape: @fit_landscape', array('@fit_landscape' => $settings['fit_landscape']?'true' : 'false'));

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 */
function supersized_formatter_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  if (empty($items)) {
    return;
  }

  $field_name = $instance['field_name'];
  $entity_type_info = entity_get_info($entity_type);
  $entity_id = $entity->{$entity_type_info['entity keys']['id']};

  $settings = $display['settings'];
  $images = array();
  foreach ($items as $image) {
    $img = array(
      'image' => file_create_url($image['uri']),
      'title' => $image['title'],
      'alt' => $image['alt'],
    );
    $images[] = $img;
  }
  $settings['images'] = json_encode($images);

  $path = libraries_get_path('supersized');
  $module_path = drupal_get_path('module', 'supersized_formatter');

  drupal_add_css($path . '/core/css/supersized.core.css');
  drupal_add_js($path . '/core/js/supersized.core.3.2.1.js');
  drupal_add_js(array('supersized_formatter' => $settings), 'setting');
  drupal_add_js($module_path . '/js/supersized-init.js');

  $element = array();
  $element['#theme'] = 'supersized_formatter';

  return $element;
}

/**
 * Implements hook_theme().
 */
function supersized_formatter_theme() {
  return array(
    'supersized_formatter' => array(
      'variables' => array(
        'images' => NULL,
        'field_name' => NULL,
      ),
      'template' => 'supersized_formatter',
    ),
  );
}
