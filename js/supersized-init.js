(function($){
    Drupal.behaviors.supersized_formatter = {
        attach: function (context){
            var settings = Drupal.settings.supersized_formatter;
            $(document).ready(function() {
                $.supersized({
                    slides : eval(settings.images),
                    start_slide : parseInt(settings.start_slide),
                    fit_always : parseInt(settings.fit_always),
                    new_window : parseInt(settings.new_window),
                    fit_landscape : parseInt(settings.fit_landscape),
                    fit_portrait : parseInt(settings.fit_portrait),
                    horizontal_center : parseInt(settings.horizontal_center),
                    vertical_center : parseInt(settings.vertical_center),
                    image_protect : parseInt(settings.image_protect),
                    min_height : parseInt(settings.min_height),
                    min_width : parseInt(settings.min_width)
                });
            });
        }
    }
})(jQuery);
